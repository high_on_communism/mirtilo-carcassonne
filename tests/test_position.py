import pytest
from model import \
    Position, PlayedPiece, TerrainType, InvalidAdjacentTilesException, UnconnectedTileException, FilledTileException

TOP_LEFT_CURVE_ROAD = PlayedPiece(
    [TerrainType.ROAD, TerrainType.ROAD, TerrainType.GRASS, TerrainType.GRASS],
    [[0, 1], [2, 3]], "", 1
)
BOT_RIGHT_CURVE_ROAD = PlayedPiece(
    [TerrainType.GRASS, TerrainType.GRASS, TerrainType.ROAD, TerrainType.ROAD],
    [[0, 1], [2, 3]], "", 2
)
BOT_LEFT_CURVE_ROAD = PlayedPiece(
    [TerrainType.ROAD, TerrainType.GRASS, TerrainType.GRASS, TerrainType.ROAD],
    [[0, 3], [1, 2]], "", 3
)
WEST_EAST_ROAD = PlayedPiece(
    [TerrainType.ROAD, TerrainType.GRASS, TerrainType.ROAD, TerrainType.GRASS],
    [[0, 2], [1], [3]], "", 4
)
TOP_ROAD_BOT_RIGHT_CORNER_CASTLE = PlayedPiece(
    [TerrainType.GRASS, TerrainType.ROAD, TerrainType.CASTLE, TerrainType.CASTLE],
    [[0], [1, 2, 3]], "", 5
)
TOP_ROAD_BOT_LEFT_CORNER_CASTLE = PlayedPiece(
    [TerrainType.CASTLE, TerrainType.ROAD, TerrainType.GRASS, TerrainType.CASTLE],
    [[0, 1, 3], [2]], "", 6
)
MONASTERY = PlayedPiece(
    [TerrainType.GRASS] * 4,
    [[0, 1, 2, 3]], "", 7, True
)


def test_empty_pos_is_valid():
    """
    Empty position is always valid.
    """
    try:
        Position([])
    except Exception:
        pytest.fail("Empty position should not raise any errors.")


def test_pos_with_one_piece_is_valid():
    """
    Position with a single piece is always valid.
    """
    try:
        Position([], {TOP_LEFT_CURVE_ROAD: [None, None, None, None]})
    except Exception:
        pytest.fail("Position with only one piece should not raise any errors.")


def test_simple_pos_is_valid_2pieces():
    """
    Just two tiles properly connected is a valid position.
    """
    try:
        Position([], {
            TOP_LEFT_CURVE_ROAD: [WEST_EAST_ROAD, None, None, None]
            , WEST_EAST_ROAD: [None, None, TOP_LEFT_CURVE_ROAD, None]
        })
    except Exception:
        pytest.fail("Two tiles properly connected should be a valid position.")
    

def test_simple_pos_is_invalid_2pieces():
    """
    Just two tiles but with mismatching faces is an invalid position.
    """
    with pytest.raises(InvalidAdjacentTilesException):
        Position([], {
            TOP_LEFT_CURVE_ROAD: [None, WEST_EAST_ROAD, None, None]
            , WEST_EAST_ROAD: [None, None, None, TOP_LEFT_CURVE_ROAD]
        })


def test_simple_pos_is_valid_4pieces():
    """
    Just 4 tiles properly connected is a valid position.
    """
    try:
        Position([], {
            BOT_RIGHT_CURVE_ROAD: [None, None, BOT_LEFT_CURVE_ROAD, TOP_ROAD_BOT_RIGHT_CORNER_CASTLE]
            , BOT_LEFT_CURVE_ROAD: [BOT_RIGHT_CURVE_ROAD, None, None, TOP_ROAD_BOT_LEFT_CORNER_CASTLE]
            , TOP_ROAD_BOT_LEFT_CORNER_CASTLE: [TOP_ROAD_BOT_RIGHT_CORNER_CASTLE, BOT_LEFT_CURVE_ROAD, None, None]
            , TOP_ROAD_BOT_RIGHT_CORNER_CASTLE: [None, BOT_RIGHT_CURVE_ROAD, TOP_ROAD_BOT_LEFT_CORNER_CASTLE, None]
        })
    except Exception:
        pytest.fail("Four tiles properly connected should be a valid position.")


def test_simple_pos_is_invalid_4pieces():
    """
    Just four tiles but with one mismatching connection is an invalid position.
    """
    with pytest.raises(InvalidAdjacentTilesException):
        Position([], {
            BOT_RIGHT_CURVE_ROAD: [None, None, BOT_LEFT_CURVE_ROAD, TOP_ROAD_BOT_RIGHT_CORNER_CASTLE]
            , BOT_LEFT_CURVE_ROAD: [BOT_RIGHT_CURVE_ROAD, None, None, TOP_LEFT_CURVE_ROAD]
            , TOP_LEFT_CURVE_ROAD: [TOP_ROAD_BOT_RIGHT_CORNER_CASTLE, BOT_LEFT_CURVE_ROAD, None, None]
            , TOP_ROAD_BOT_RIGHT_CORNER_CASTLE: [None, BOT_RIGHT_CURVE_ROAD, TOP_LEFT_CURVE_ROAD, None]
        })
        
        
def test_simple_pos_with_unconnected_piece_is_invalid():
    """
    Just 4 tiles properly connected plus an unconnected one is an invalid position.
    """
    with pytest.raises(UnconnectedTileException):
        Position([], {
            BOT_RIGHT_CURVE_ROAD: [None, None, BOT_LEFT_CURVE_ROAD, TOP_ROAD_BOT_RIGHT_CORNER_CASTLE]
            , BOT_LEFT_CURVE_ROAD: [BOT_RIGHT_CURVE_ROAD, None, None, TOP_ROAD_BOT_LEFT_CORNER_CASTLE]
            , TOP_ROAD_BOT_LEFT_CORNER_CASTLE: [TOP_ROAD_BOT_RIGHT_CORNER_CASTLE, BOT_LEFT_CURVE_ROAD, None, None]
            , TOP_ROAD_BOT_RIGHT_CORNER_CASTLE: [None, BOT_RIGHT_CURVE_ROAD, TOP_ROAD_BOT_LEFT_CORNER_CASTLE, None]
            , WEST_EAST_ROAD: [None, None, None, None]
        })


def test_add_valid_tile_to_position():
    pos = Position([], {
        BOT_RIGHT_CURVE_ROAD: [None, None, BOT_LEFT_CURVE_ROAD, TOP_ROAD_BOT_RIGHT_CORNER_CASTLE]
        , BOT_LEFT_CURVE_ROAD: [BOT_RIGHT_CURVE_ROAD, None, None, TOP_ROAD_BOT_LEFT_CORNER_CASTLE]
        , TOP_ROAD_BOT_LEFT_CORNER_CASTLE: [TOP_ROAD_BOT_RIGHT_CORNER_CASTLE, BOT_LEFT_CURVE_ROAD, None, None]
        , TOP_ROAD_BOT_RIGHT_CORNER_CASTLE: [None, BOT_RIGHT_CURVE_ROAD, TOP_ROAD_BOT_LEFT_CORNER_CASTLE, None]
    })
    try:
        pos.add_piece([None, None, BOT_RIGHT_CURVE_ROAD, None], MONASTERY)
        assert len(pos.pieces) == 5
    except Exception:
        pytest.fail("Adding a tile properly connected to a valid position should not fail.")


def test_add_unconnected_tile_to_position():
    pos = Position([], {
        BOT_RIGHT_CURVE_ROAD: [None, None, BOT_LEFT_CURVE_ROAD, TOP_ROAD_BOT_RIGHT_CORNER_CASTLE]
        , BOT_LEFT_CURVE_ROAD: [BOT_RIGHT_CURVE_ROAD, None, None, TOP_ROAD_BOT_LEFT_CORNER_CASTLE]
        , TOP_ROAD_BOT_LEFT_CORNER_CASTLE: [TOP_ROAD_BOT_RIGHT_CORNER_CASTLE, BOT_LEFT_CURVE_ROAD, None, None]
        , TOP_ROAD_BOT_RIGHT_CORNER_CASTLE: [None, BOT_RIGHT_CURVE_ROAD, TOP_ROAD_BOT_LEFT_CORNER_CASTLE, None]
    })
    with pytest.raises(UnconnectedTileException):
        pos.add_piece([None, None, None, None], MONASTERY)
    try:
        empty_pos = Position([])
        empty_pos.add_piece([None, None, None, None], MONASTERY)  # unconnected piece is valid on an empty position
        assert len(empty_pos.pieces) == 1
    except Exception:
        pytest.fail("Adding a tile to an empty position should always be valid.")


def test_add_invalid_adjacent_tile_to_position():
    pos = Position([], {
        BOT_RIGHT_CURVE_ROAD: [None, None, BOT_LEFT_CURVE_ROAD, TOP_ROAD_BOT_RIGHT_CORNER_CASTLE]
        , BOT_LEFT_CURVE_ROAD: [BOT_RIGHT_CURVE_ROAD, None, None, TOP_ROAD_BOT_LEFT_CORNER_CASTLE]
        , TOP_ROAD_BOT_LEFT_CORNER_CASTLE: [TOP_ROAD_BOT_RIGHT_CORNER_CASTLE, BOT_LEFT_CURVE_ROAD, None, None]
        , TOP_ROAD_BOT_RIGHT_CORNER_CASTLE: [None, BOT_RIGHT_CURVE_ROAD, TOP_ROAD_BOT_LEFT_CORNER_CASTLE, None]
    })
    with pytest.raises(InvalidAdjacentTilesException):
        pos.add_piece([None, TOP_ROAD_BOT_LEFT_CORNER_CASTLE, None, None], MONASTERY)


def test_add_tile_in_filled_spot_is_invalid():
    pos = Position([], {
        BOT_RIGHT_CURVE_ROAD: [None, None, BOT_LEFT_CURVE_ROAD, TOP_ROAD_BOT_RIGHT_CORNER_CASTLE]
        , BOT_LEFT_CURVE_ROAD: [BOT_RIGHT_CURVE_ROAD, None, None, TOP_ROAD_BOT_LEFT_CORNER_CASTLE]
        , TOP_ROAD_BOT_LEFT_CORNER_CASTLE: [TOP_ROAD_BOT_RIGHT_CORNER_CASTLE, BOT_LEFT_CURVE_ROAD, None, None]
        , TOP_ROAD_BOT_RIGHT_CORNER_CASTLE: [None, BOT_RIGHT_CURVE_ROAD, TOP_ROAD_BOT_LEFT_CORNER_CASTLE, None]
    })
    with pytest.raises(FilledTileException):
        pos.add_piece([None, BOT_RIGHT_CURVE_ROAD, TOP_ROAD_BOT_LEFT_CORNER_CASTLE, None], MONASTERY)
