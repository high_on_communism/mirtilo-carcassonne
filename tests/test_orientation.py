from model import Orientation


def test_opposite():
    assert Orientation.WEST.value == Orientation.opposite(Orientation.EAST.value)
    assert Orientation.NORTH.value == Orientation.opposite(Orientation.SOUTH.value)
    assert Orientation.EAST.value == Orientation.opposite(Orientation.WEST.value)
    assert Orientation.SOUTH.value == Orientation.opposite(Orientation.NORTH.value)

