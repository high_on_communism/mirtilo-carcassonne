import os

from model import Piece, PlayedPiece, TerrainType

BOT_LEFT_CURVE_ROAD = Piece(
    [TerrainType.ROAD, TerrainType.GRASS, TerrainType.GRASS, TerrainType.ROAD],
    [[0, 3], [1, 2]], ""
)
TWO_ROADS_INTERSECTION = Piece(
    [TerrainType.ROAD, TerrainType.ROAD, TerrainType.ROAD, TerrainType.ROAD],
    [[0, 2], [1, 3]], ""
)
MONASTERY = Piece(
    [TerrainType.GRASS, TerrainType.GRASS, TerrainType.GRASS, TerrainType.GRASS],
    [[0, 1, 2, 3]], "", True
)
CCC = Piece(
    [TerrainType.CASTLE, TerrainType.CASTLE, TerrainType.CASTLE, TerrainType.GRASS],
    [[0, 1, 2], [3]], ""
)
CCC_WITH_ROAD = Piece(
    [TerrainType.CASTLE, TerrainType.CASTLE, TerrainType.CASTLE, TerrainType.ROAD],
    [[0, 1, 2, ], [3]], ""
)

PLAYED_TOP_LEFT_CURVE_ROAD = PlayedPiece(
    [TerrainType.ROAD, TerrainType.ROAD, TerrainType.GRASS, TerrainType.GRASS],
    [[0, 1], [2, 3]], "", 1
)
PLAYED_BOT_RIGHT_CURVE_ROAD = PlayedPiece(
    [TerrainType.GRASS, TerrainType.GRASS, TerrainType.ROAD, TerrainType.ROAD],
    [[0, 1], [2, 3]], "", 2
)


def _are_pieces_equal(p1, p2):
    return p1.faces == p2.faces and p1.conns == p2.conns and p1.has_monastery == p2.has_monastery
    # not testing for image filepaths as it's irrelevant for any functional purposes and
    # besides who says it's not the same image at two different filepaths? :)


def test_read_pieces_from_json():
    pieces = Piece.read_pieces_from_json(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), "test_assets/pieces.json"))

    assert _are_pieces_equal(pieces[0], BOT_LEFT_CURVE_ROAD)
    assert _are_pieces_equal(pieces[1], TWO_ROADS_INTERSECTION)
    assert _are_pieces_equal(pieces[2], MONASTERY)
    assert _are_pieces_equal(pieces[3], CCC)
    assert _are_pieces_equal(pieces[4], CCC_WITH_ROAD)


def test_played_piece_identity():
    assert PLAYED_TOP_LEFT_CURVE_ROAD != 1
    assert PLAYED_TOP_LEFT_CURVE_ROAD != PLAYED_BOT_RIGHT_CURVE_ROAD
    assert PLAYED_TOP_LEFT_CURVE_ROAD == PLAYED_TOP_LEFT_CURVE_ROAD
    assert PLAYED_BOT_RIGHT_CURVE_ROAD == PLAYED_BOT_RIGHT_CURVE_ROAD

