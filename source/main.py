import sys
from PySide6 import QtWidgets

from widgets import EditorScreen, MainMenuScreen


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()

        self.central_widget = QtWidgets.QStackedWidget()
        self.setCentralWidget(self.central_widget)

        self.main_screen = MainMenuScreen()
        self.editor_screen = EditorScreen()
        self.central_widget.addWidget(self.main_screen)
        self.central_widget.addWidget(self.editor_screen)

        self.central_widget.setCurrentWidget(self.main_screen)

        self.main_screen.clicked.connect(lambda: self.central_widget.setCurrentWidget(self.editor_screen))
        self.editor_screen.clicked.connect(lambda: self.central_widget.setCurrentWidget(self.main_screen))

        self.setWindowTitle("Carcassonne")
        self.resize(1200, 720)


def main():
    app = QtWidgets.QApplication(sys.argv)
    my_window = MainWindow()
    my_window.show()
    sys.exit(app.exec())


if __name__ == "__main__":
    main()
