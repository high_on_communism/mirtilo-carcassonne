import os

from model import Piece, PlayedPiece, Position


class PiecesController:

    def __init__(self):
        base_path = os.path.dirname(os.path.abspath(__file__)).split("source", 1)[0]
        pieces_path = os.path.join(base_path, "assets/pieces.json")
        self.pieces = Piece.read_pieces_from_json(pieces_path)

        self.current_position = Position([])



