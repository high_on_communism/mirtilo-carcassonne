from enum import Enum, unique


@unique
class Orientation(Enum):
    WEST = 0
    NORTH = 1
    EAST = 2
    SOUTH = 3

    @staticmethod
    def opposite(orientation):
        return (orientation + 2) % 4


@unique
class TerrainType(Enum):
    CASTLE = 'C'
    GRASS = 'G'
    ROAD = 'R'


@unique
class Player(Enum):
    BLUE = 0
    RED = 1
    BLACK = 2
    PINK = 3
    GREEN = 4
    YELLOW = 5
