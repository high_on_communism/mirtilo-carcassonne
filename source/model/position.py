import logging

from .constants import Orientation, Player, TerrainType
from .piece import PlayedPiece


class FilledTileException(BaseException):
    def __init__(self, message=""):
        super().__init__(message)


class InvalidAdjacentTilesException(BaseException):
    def __init__(self, tile_pivot: PlayedPiece, idx: int, tile_neighbour: PlayedPiece):
        super().__init__(
            f"Face of neighbour at {Orientation(idx).name} is {tile_neighbour.faces[Orientation.opposite(idx)].name} and doesn't match with {tile_pivot.faces[idx].name}."
        )


class UnconnectedTileException(BaseException):
    def __init__(self, message=""):
        super().__init__(message)


"""
A graph of played pieces is always assumed to be a dict[PlayedPiece, list[PlayedPiece]] where
the list of connected pieces is always in the same order of Orientation Enum, not having a piece
in that direction is represented by None.
"""


class Position:
    """
    A position is simply a graph of played pieces and their positions.
    Building on that, it provides an interface for the score of each player,
    number of castles, roads and such.
    Basically any metadata regarding the current position that might be useful
    will be provided here.
    """

    def __init__(self, players: list[Player], pieces: dict[PlayedPiece, list[PlayedPiece]] = None):
        self.players = players
        self.pieces = pieces if pieces else {}
        self._validate()

    @classmethod
    def from_json(cls, json):
        return cls(json.items())

    def _validate(self):
        if len(self.pieces) < 2:
            return  # an empty position or a position with only one piece is always valid
        for piece, neighbours in self.pieces.items():
            if not any(neighbours):
                raise UnconnectedTileException("Piece must have at least one neighbour.")
            for idx, neighbour in enumerate(neighbours):
                if neighbour and piece.faces[idx] != neighbour.faces[Orientation.opposite(idx)]:
                    raise InvalidAdjacentTilesException(piece, idx, neighbour)

    def _travelsal(self, should_go_to_neighbour=lambda _: True, func=lambda _: None, start_piece=None, visited=None):
        if not self.pieces:
            return  # there are no pieces to traverse
        if start_piece is None:
            start_piece = next(iter(self.pieces))
        if visited is None:
            visited = set()

        def get_next_neighbour(ns):
            return next((p for p in ns if p and p not in visited and should_go_to_neighbour(p)), None)

        func(start_piece)

        neighbours = self.pieces[start_piece]
        next_piece = get_next_neighbour(neighbours)
        visited.add(start_piece)
        while next_piece:
            self._travelsal(next_piece, visited)
            next_piece = get_next_neighbour(neighbours)

    def add_piece(self, neighbours: [PlayedPiece], piece: PlayedPiece):
        if not any(neighbours) and self.pieces:  # cannot add an unconnected piece to a non-empty position
            raise UnconnectedTileException("Piece must have at least one neighbour.")

        for idx, neighbour in enumerate(neighbours):
            opposite_idx = Orientation.opposite(idx)
            if neighbour is not None:
                if self.pieces.get(neighbour)[opposite_idx] is not None:
                    raise FilledTileException()
                if piece.faces[idx] != neighbour.faces[opposite_idx]:
                    raise InvalidAdjacentTilesException(piece, idx, neighbour)

        self.pieces[piece] = [None, None, None, None]
        for idx, neighbour in enumerate(neighbours):
            if neighbour is not None:
                opposite_idx = Orientation.opposite(idx)
                self.pieces.get(piece)[idx] = neighbour
                self.pieces.get(neighbour)[opposite_idx] = piece
