import logging
import json

from .constants import Orientation, TerrainType, Player


class Piece:
    """
    Faces are assumed to be a list of 4 elements in the same order as Orientation enum.
    """

    def __init__(self, faces: list[TerrainType],
                 conns: list[list[int]],
                 # each sublist is a group of connected faces, you can have up to 4 sublists with one face each
                 image_filepath: str,
                 has_monastery: bool = False):
        self.faces = faces
        self.conns = conns
        self.has_monastery = has_monastery
        self.image_filepath = image_filepath

    @classmethod
    def read_pieces_from_json(cls, filepath):
        pieces = []
        with open(filepath) as f:
            data = json.load(f)
            for piece in data:
                faces = [TerrainType(f) for f in piece["faces"]]
                pieces.append(cls(faces, piece["connections"], piece["image"], piece.get("has_monastery", False)))
            return pieces


class PlayedPiece(Piece):
    """
    It represents a played piece, meaning it can have a pawn on it. It must have an id for hashing purposes
    and it's the caller's responsibility to guarantee that two different instances of PlayedPiece belonging
    to the same instance of Position do not share the same id.
    """

    def __init__(self, faces, conns, image_filepath, id_in_pos: int, has_monastery=False,
                 played_pawn: tuple[Orientation, Player] = None):
        """
        Use None for the Orientation in case the played pawn was played on the church.
        """
        super().__init__(faces, conns, image_filepath, has_monastery)
        # the id is only used to make hashing simpler so that creating a graph of pieces is not so costly
        self.id_in_pos = id_in_pos
        self.played_pawn = played_pawn

    def __hash__(self) -> int:
        return hash(self.id_in_pos)

    def __eq__(self, other: object) -> bool:
        return type(self) == type(other) and self.id_in_pos == other.id_in_pos
