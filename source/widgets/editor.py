from PySide6 import QtCore, QtWidgets, QtGui

from .piece import PieceUI
from controller import PiecesController


class Editor(QtWidgets.QWidget):

    clicked = QtCore.Signal()

    def __init__(self):
        super().__init__()

        self.controller = PiecesController()

        self.layout = QtWidgets.QGridLayout()
        self.add_piece(PieceUI(self.controller.pieces[0]))
        self.setLayout(self.layout)

    def add_piece(self, piece: PieceUI):
        self.layout.addWidget(piece)


class EditorScreen(QtWidgets.QWidget):

    clicked = QtCore.Signal()

    def __init__(self):
        super().__init__()

        # self.controller = PiecesController()
        self.editor = Editor()

        self.layout = QtWidgets.QVBoxLayout()

        toolbar = QtWidgets.QToolBar()
        back_to_main_action = QtGui.QAction("Main Menu", self)
        back_to_main_action.triggered.connect(self.clicked.emit)
        toolbar.addAction(back_to_main_action)
        save_action = QtGui.QAction("Save Position", self)
        save_action.triggered.connect(self.on_save_position_clicked)
        toolbar.addAction(save_action)

        self.layout.addWidget(toolbar)

        self.layout.addWidget(self.editor)
        self.setLayout(self.layout)

    def on_save_position_clicked(self):
        print("on_save_position_clicked")