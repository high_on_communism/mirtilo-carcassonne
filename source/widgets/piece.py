import os

from PySide6 import QtCore, QtWidgets, QtGui

from model import PlayedPiece


PIECE_SIZE = (105, 105)


class PieceUI(QtWidgets.QWidget):

    clicked = QtCore.Signal()

    def __init__(self, piece: PlayedPiece):
        super().__init__()

        self.label = QtWidgets.QLabel(self)
        self.pixmap = QtGui.QPixmap(f"assets/{piece.image_filepath}")
        assert not self.pixmap.isNull()
        self.label.setPixmap(self.pixmap)
        self.label.resize(PIECE_SIZE[0], PIECE_SIZE[1])
