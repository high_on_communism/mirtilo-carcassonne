from PySide6 import QtCore, QtWidgets


class MainMenuScreen(QtWidgets.QWidget):

    clicked = QtCore.Signal()

    def __init__(self):
        super().__init__()
        layout = QtWidgets.QVBoxLayout()
        button = QtWidgets.QPushButton(text='Editor')
        layout.addWidget(button)
        self.setLayout(layout)
        button.clicked.connect(self.clicked.emit)